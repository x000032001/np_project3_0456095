#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include "Logger.h"
#include "HtmlWrapper.h"
#include "util.h"

using namespace std;

int getConnSocket(string host, string port);

int main()
{
    map<string, string> querys = queryExtract();

    int epoll_fd = epoll_create(10);
    struct epoll_event event;
    event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET;

    map<int, int> map;
    FILE* files[6] = {};
    int conn_count = 0;
    string header_msg[5] = {};
    for(int i = 1; i <= 5; ++i) {
        int rc = getConnSocket(querys["h"+to_string(i)], querys["p"+to_string(i)]);
        FILE* file = fopen(querys["f"+to_string(i)].c_str(), "r");
        if(rc < 0) {
            if(querys["h"+to_string(i)] =="" || querys["p"+to_string(i)] == "")
                header_msg[i-1] = "Missing setting";
            else
                header_msg[i-1] = "[FAIL] Connect to " + querys["h"+to_string(i)] + "/" + querys["p"+to_string(i)];

            slogf(WARN, "%d open conn failed\n",i);
            if(file)    fclose(file);
            continue;
        }
        if(!file) {
            header_msg[i-1] = "[FAIL] Open file " + querys["h"+to_string(i)];
            slogf(WARN, "%d open file failed\n",i);
            close(rc);
            continue;
        }

        event.data.fd = rc;
        epoll_ctl(epoll_fd, EPOLL_CTL_ADD, rc, &event);
        conn_count++;
        files[i] = file;
        map[rc] = i;

        header_msg[i-1] = querys["h"+to_string(i)] + "/" + querys["p"+to_string(i)];
    }

    HtmlWrapper::Init(header_msg);
    slogf(INFO, "Setup Ok!");

    char write_buffer[6][1024] = {};
    char *write_ptr[6] = {};

    while (conn_count != 0) {
        struct epoll_event events[10];
        int rc = epoll_wait(epoll_fd, events, 10, -1); // -1 for block until anyone ready
        if (rc < 0) {
            printf("epoll_wait err\n");
            exit(0);
        }
        for (int i = 0; i < rc; ++i) {
            int fd = events[i].data.fd;
            int index = map[fd];
            /* handle disconnected events */
            if (events[i].events & EPOLLERR || events[i].events & EPOLLHUP || events[i].events & EPOLLRDHUP ) {
                char msg[] = "Disconnected";
                HtmlWrapper::Print(index-1,msg,sizeof(msg)-1);
                conn_count--;
				close(fd);
                fflush(stdout);
                continue;
            }

            /* handle readable*/
            if (events[i].events & EPOLLIN) {
				slogf(DEBUG, "EPOLLIN\n");
				bool hasPrompt = false;
                while (1) {
                    char buff[513] = {};
                    int len = read(fd, buff, 512);
                    if (len == 0)
                        break;
					if (len < 0) {
						slogf(WARN, "%s\n", strerror(errno));
						break;
					}
                    HtmlWrapper::Print(index-1,buff,len);
                    fflush(stdout);

					for(int i = 0; i < 511; ++i) {
						if(buff[i] == '%' && buff[i+1] == ' ')
							hasPrompt = true;
					}
                }

				if(hasPrompt && files[index]) {
					event.data.fd = fd;
					slogf(WARN, "epoll ctl mod\n");
					event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET;
					epoll_ctl(epoll_fd, EPOLL_CTL_MOD, fd, &event);
				}
            }

            /* 觸發情況
             * 1. 連線第一次建立
             * 2. 緩衝區由滿 -> 可寫
             * 3. 重新被加入epoll_fd
             * 總之就是writable
             */
            if (events[i].events & EPOLLOUT) {
				slogf(DEBUG, "EPOLLOUT\n");
				event.data.fd = fd;
				event.events = EPOLLIN | EPOLLRDHUP | EPOLLET;
				epoll_ctl(epoll_fd, EPOLL_CTL_MOD, fd, &event);
 
                if(write_ptr[index] == 0) {
                    /* buffer為空 需要新的input */
                    if(NULL == fgets(write_buffer[index], 1023, files[index])) {
                        /* 檔案已經EOF */
						fclose(files[index]);
						files[index] = NULL;
                        slogf(INFO, "#%d encounter EOF\n",index);
                        continue;
                    } else {
						slogf(INFO, "fget() =%d %s\n",strlen(write_buffer[index]), write_buffer[index]);
                        write_ptr[index] = write_buffer[index];
                        int len = write(fd, write_ptr[index], strlen(write_ptr[index]));
                        if(len == strlen(write_ptr[index])) { // 所有資料都寫入成功
                            write_ptr[index] = 0;
                            if(strlen(write_buffer[index]) == 1022) {
                                event.data.fd = fd;
                                event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET;
                                epoll_ctl(epoll_fd, EPOLL_CTL_MOD, fd, &event);
                            }
                        } else
                            write_ptr[index] += len;
                    }
                } else {
                    int len = write(fd, write_ptr[index], strlen(write_ptr[index]));
                    if(len == strlen(write_ptr[index])) { // 所有資料都寫入成功
                        write_ptr[index] = 0;
						if(strlen(write_buffer[index]) == 1022) {
                            event.data.fd = fd;
                            event.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET;
                            epoll_ctl(epoll_fd, EPOLL_CTL_MOD, fd, &event);
                        }
                    } else
                        write_ptr[index] += len;
                }
            }
        }
    }

    HtmlWrapper::Final();

    return 0;
}

int getConnSocket(string host, string port)
{
    if (host == "" || port == "")
        return -1;
    printf("Connect to %s/%s\n",host.c_str(), port.c_str());
    fflush(stdout);
    int p = stol(port);

    int c_fd, flags, ret;
    struct sockaddr_in s_addr;
    memset(&s_addr, 0, sizeof(s_addr));

    struct hostent *hptr;  
    if ((hptr = gethostbyname(host.c_str())) == NULL)   
    {   
        return -1;
    }  

    memcpy(&s_addr.sin_addr, hptr->h_addr_list[0], hptr->h_length);
    s_addr.sin_family = hptr->h_addrtype;
    s_addr.sin_port = htons(p);


    if ((c_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("create socket fail.\n");
        exit(0);
    }
    flags = fcntl(c_fd, F_GETFL, 0);
    if (flags < 0) {
        perror("get socket flags fail.\n");
        return -1;
    }

    if (fcntl(c_fd, F_SETFL, flags | O_NONBLOCK) < 0) {
        perror("set socket O_NONBLOCK fail.\n");
        return -1;
    }
    ret = connect(c_fd, (struct sockaddr*)&s_addr, sizeof(struct sockaddr));
    if (ret < 0) {
        if (errno == EINPROGRESS) {
            return c_fd;
        } else {
            perror("connect remote server fail.\n");
            printf("%d\n", errno);
            exit(0);
        }
    }


    return c_fd;
}


