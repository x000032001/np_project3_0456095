#include "util.h"

map<string, string> queryExtract()
{
    char* ptr = secure_getenv("QUERY_STRING");
    if (!ptr) {
        slogf(ERROR, "Abort! No Query String");
    }

    string query_str(ptr);
    auto query_arr = split(query_str, "&");

    map<string, string> querys;

    for (auto& str : query_arr) {
        auto tmp = split(str, "=");
        if (tmp.size() == 2) {
            querys[tmp[0]] = tmp[1];
        }
    }

    return querys;
}


vector<string> split(const string& source, const string& delim)
{
    vector<string> ans;
    size_t begin_pos = 0, end_pos = source.find(delim);
    while (end_pos != string::npos) {
        ans.push_back(source.substr(begin_pos, end_pos - begin_pos));
        begin_pos = end_pos + delim.size();
        end_pos = source.find(delim, begin_pos);
    }
    ans.push_back(source.substr(begin_pos, end_pos - begin_pos));
    return ans;
}

bool hasEnding(std::string const& fullString, std::string const& ending)
{
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}
