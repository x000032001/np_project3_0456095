#pragma once

#include <vector>
#include <map>
#include "Logger.h"

using namespace std;

map<string, string> queryExtract();
vector<string> split(const string& source, const string& delim);
