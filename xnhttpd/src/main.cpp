#include "common.h"
#include "TCPServer.h"
#include "Worker.h"

int main(int argc, char **argv)
{
    SET_LOG_LEVEL(DEBUG);

    int port = PORT;
    string webroot = WEB_ROOT;

    for( int i = 1; i < argc ; ++i ) {
        int res = strtol(argv[i],NULL,10);
        if( res != 0 ) {
            port = res;
            continue;
        }

        struct stat sb;
        if (stat(argv[i], &sb) == 0 && S_ISDIR(sb.st_mode))
            webroot = string(argv[i]);
    }

    TCPServer tcpServ;
    tcpServ.Init(port);

    while(1) {
        int connfd = tcpServ.GetConn();
        if(fork()) {
            slogf(INFO, "Spawn worker for %d\n",connfd);
            close(connfd);
        } else {
            serve(webroot, connfd);
            break;
        }
    }

    return 0;
}
