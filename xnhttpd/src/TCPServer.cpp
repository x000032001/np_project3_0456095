#include "TCPServer.h"

TCPServer::TCPServer()
{
    sockfd = -1;
}

int TCPServer::Init(int port)
{
    if (0 > (sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
        slogf(ERROR, "socket() %s\n", strerror(errno));
    }

    struct sockaddr_in sAddr;

    bzero((char*)&sAddr, sizeof(sAddr));
    sAddr.sin_family = AF_INET;
    sAddr.sin_addr.s_addr = INADDR_ANY;
    sAddr.sin_port = htons(port);

    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sAddr, sizeof(sAddr));

    if (0 > bind(sockfd, (struct sockaddr*)&sAddr, sizeof(sAddr))) {
        slogf(ERROR, "bind() %s\n", strerror(errno));
    }

    if (0 > listen(sockfd, 5)) {
        slogf(ERROR, "listen() %s\n", strerror(errno));
    }

    make_socket_non_blocking(sockfd);

    slogf(INFO, "Listen Ok at Port %d: return sockfd %d\n",port,sockfd);

    epoll_fd = epoll_create(50);
    if (epoll_fd < 0)
        slogf(ERROR, "epoll_create %s\n", strerror(errno));

    event.data.fd = sockfd;
    event.events = EPOLLIN | EPOLLET;
    if (0 > epoll_ctl(epoll_fd, EPOLL_CTL_ADD, sockfd, &event))
        slogf(ERROR, "epoll_ctl %s\n", strerror(errno));
    events = (epoll_event*)calloc(50, sizeof(event));

    return T_Success;
}

int TCPServer::GetConn()
{
    return recv_data_from_socket();
}

int TCPServer::make_socket_non_blocking(int sfd)
{
    int flags, s;

    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        slogf(ERROR, "fcntl");
        return T_Failure;
    }

    flags |= O_NONBLOCK;
    s = fcntl(sfd, F_SETFL, flags);
    if (s == -1) {
        slogf(ERROR, "fcntl");
        return T_Failure;
    }

    return T_Success;
}

int TCPServer::recv_data_from_socket()
{
    int rc;
    while (1) {
        rc = epoll_wait(epoll_fd, events, 50, -1);
        if (rc == -1) {
            if (errno == EINTR)
                continue;
            else {
                slogf(ERROR, "epoll_wait %s\n", strerror(errno));
            }
        } else {
            break;
        }
    }
    for (int i = 0; i < rc; ++i) {
        if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || !(events[i].events & EPOLLIN)) {
            slogf(WARN, "Disconnected %d\n", events[i].data.fd);
            close(events[i].data.fd);
            continue;
        }
        if (sockfd == events[i].data.fd) {
            while (1) {
                struct sockaddr_in cAddr;
                socklen_t len = sizeof(cAddr);
                bzero((char*)&cAddr, sizeof(cAddr));

                /* accept new user */
                int connfd = accept(sockfd, (struct sockaddr*)&cAddr, &len);
                if (connfd < 0) {
                    if (errno == EAGAIN || errno == EWOULDBLOCK)
                        break;
                    slogf(ERROR, "accept() %s\n", strerror(errno));
                }

                slogf(DEBUG, "Connection %s/%d\n", inet_ntoa(cAddr.sin_addr), ntohs(cAddr.sin_port));
                return connfd;
            }
        }
    }
    return -rc;
}

