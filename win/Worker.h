#pragma once

#include <cstdio>
#include <cstdlib>
#include <vector>
#include "Http.h"
using namespace std;

#define WM_CGI_NOTIFY (WM_USER + 5)
#define ROOT "C:\\Users\\num\\Dropbox\\NP\\hw3"

class Server {
public:
	FILE* file;
	SOCKET sock;
	int index;
};

class Worker {
public:
	map<SOCKET, Server> servers;
	int conns;

	Worker() {}
	Worker(SOCKET s, Request r) : sock(s), req(r) { conns = 0; }
	void Init();
	void Connect(SOCKET s);
	void Read(SOCKET s);
	void Write(SOCKET s);
	void Close(SOCKET s);
private:
	Request req;
	SOCKET sock;

	char write_buffer[6][1024];
	char *write_ptr[6];

	void CGI_Init(string[5]);
	void Print(int index, const char* buff, int size);
};