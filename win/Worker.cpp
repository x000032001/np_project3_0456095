#include "Worker.h"

vector<string> split(const string &source, const string &delim)
{
	vector<string> ans;
	size_t begin_pos = 0, end_pos = source.find(delim);
	while (end_pos != string::npos) {
		ans.push_back(source.substr(begin_pos, end_pos - begin_pos));
		begin_pos = end_pos + delim.size();
		end_pos = source.find(delim, begin_pos);
	}
	ans.push_back(source.substr(begin_pos, end_pos - begin_pos));
	return ans;
}

bool hasEnding(std::string const &fullString, std::string const &ending) {
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
	}
	else {
		return false;
	}
}

map<string, string> queryExtract(const char* query)
{
	string query_str(query);
	auto query_arr = split(query_str, "&");

	map<string, string> querys;

	for (auto& str : query_arr) {
		auto tmp = split(str, "=");
		if (tmp.size() == 2) {
			querys[tmp[0]] = tmp[1];
		}
	}

	return querys;
}

void Worker::Init()
{
	for (int i = 0; i < 6; ++i)
	{
		memset(write_buffer[i], 0, 1024);
		write_ptr[i] = 0;
	}

	// find query string
	auto res = split(req.uri, "?");
	string query_str;
	if (res.size() == 2) {
		query_str = res[1];
	}

	// is .cgi
	if (!hasEnding(res[0], ".cgi")) {
		string file = ROOT + res[0];
		FILE* f = fopen(file.c_str(), "r");
		if (!f) {
			//MessageBox(0, file.c_str(), "MessageBox caption", MB_OK);
			char msg[] = "HTTP/1.1 404 Not Found\r\n";
			send(sock, msg, sizeof(msg), 0);
			closesocket(sock);
			return;
		}
		char msg[] = "HTTP/1.1 200 Ok\r\nContent-Type: text/html\r\n\r\n";
		send(sock, msg, sizeof(msg), 0);
		while (1) {
			char buff[1024] = {};
			int res = fread(buff, 1, 1024, f);
			int rc = send(sock, buff, 1024, 0);
			if (res != 1024) break;
		}
		closesocket(sock);
		return;
	}

	map<string, string> querys = queryExtract(query_str.c_str());

	string msg[5];
	for (int i = 1; i <= 5; ++i) {
		if (querys["f" + to_string(i)] == "" || querys["h" + to_string(i)] == "" || querys["p" + to_string(i)] == "")
			continue;
		FILE* f = fopen(string(ROOT "\\" + querys["f" + to_string(i)]).c_str(), "r");
		if (!f) {
			continue;
		}

		SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock == INVALID_SOCKET) {
			//MessageBox(0, "socket error", "MessageBox caption", MB_OK);
			msg[i - 1] = "socket";
			return;
		}

		u_long iMode = 1;
		if (0 != ioctlsocket(sock, FIONBIO, &iMode)) {
			//MessageBox(0, "ioctlsocket error", "MessageBox caption", MB_OK);
			msg[i - 1] = "ioctlsocket";
			continue;
		}

		string host = querys["h" + to_string(i)];
		int port = stol(querys["p" + to_string(i)]);

		sockaddr_in clientService;
		clientService.sin_family = AF_INET;
		clientService.sin_addr.s_addr = inet_addr(host.c_str());
		clientService.sin_port = htons(port);
		conns++;
		if (SOCKET_ERROR == connect(sock, (SOCKADDR *)& clientService, sizeof (clientService))) {
			//MessageBox(0, "connect error", "MessageBox caption", MB_OK);
			if (WSAGetLastError() != WSAEWOULDBLOCK)
			{
				msg[i - 1] = "Connect Failed: " + to_string(WSAGetLastError());
				continue;
			}
		}

		servers[sock].file = f;
		servers[sock].sock = sock;
		servers[sock].index = i;

		msg[i - 1] = querys["h" + to_string(i)] +"/"+ querys["p" + to_string(i)];
	}

	CGI_Init(msg);
}

void Worker::Connect(SOCKET s)
{
	conns++;
	Write(s);
}

void Worker::Read(SOCKET s)
{
	int index = servers[s].index;

	bool sendCmd = false;
	while (1) {
		char buff[513] = {};
		int len = recv(s, buff, 512, 0);
		if (len <= 0)
			break;
		if (len != 512)
			sendCmd = true;
		Print(index - 1, buff, len);
	}

	if (sendCmd) Write(s);
}

void Worker::Write(SOCKET s)
{
	int index = servers[s].index;

	while (1) {

		if (write_ptr[index] == 0) {
			/* buffer為空 需要新的input */
			if (NULL == fgets(write_buffer[index], 1023, servers[s].file)) {
				/* 檔案已經EOF */
				//closesocket(s);
				shutdown(s, 1);
				return;
			}
		}
	
		write_ptr[index] = write_buffer[index];
		int len = send(s, write_ptr[index], strlen(write_ptr[index]), 0);
		if (len == strlen(write_ptr[index])) { // 所有資料都寫入成功
			write_ptr[index] = 0;
			if (write_buffer[index][strlen(write_buffer[index]) - 1] != '\n') {
				continue;
			}
			else {
				break;
			}
		}
		else {
			write_ptr[index] += len;
			break;
		}
	}
}

void Worker::Close(SOCKET s)
{
	conns--;
	char done[] = "Disconnected\r\n";
	Print(servers[s].index -1, done, sizeof(done));
	closesocket(s);
	if (!conns)
	{
		char msg[] = "</font>"
			"</body>"
			"</html>";
		send(sock, msg, sizeof(msg), 0);
		closesocket(sock);
	}
}

void Worker::CGI_Init(string header_msg[5])
{
	char buff[2048] = {};
	int rc = sprintf(buff, 
		"HTTP/1.1 200 Ok\r\n"
		"Content-Type: text/html\r\n\r\n"
		"<html>"
		"<head>"
		"<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />"
		"<title>Network Programming Homework 3</title>"
		"</head>"
		"<body bgcolor=#336699>"
		"<font face='Courier New' size=2 color=#FFFF99>"
		"<table width='800' border='1'>"
		"<tr>"
		"<td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>"
		"<tr>"
		"<td valign='top' id='m0'></td><td valign='top' id='m1'></td><td valign='top' id='m2'></td><td valign='top' id='m3'></td><td valign='top' id='m4'></td></tr>"
		"</table>"
		, header_msg[0].c_str(), header_msg[1].c_str(), header_msg[2].c_str(), header_msg[3].c_str(), header_msg[4].c_str());
	
	send(sock, buff, strlen(buff), 0);
}

void Worker::Print(int index, const char* buff, int size)
{
	char pre[128] = {};
	sprintf(pre,"<script>document.all['m%d'].innerHTML += '", index);
	send(sock, pre, strlen(pre), 0);

	map<char, string> escape = {
		{ '\r', "" },
		{ '\n', "<br>" },
		{ '\"', "&quot;" },
		{ '\'', "&apos;" },
		{ '&', "&amp;" },
		{ '<', "&lt;" },
		{ '>', "&gt;" },
		{ ' ', "&nbsp;" }
	};
	for (int i = 0; i < size; ++i) {
		auto it = escape.find(buff[i]);
		if (it != escape.end()) {
			send(sock, escape[buff[i]].c_str(), escape[buff[i]].size(), 0);
		}
		else {
			send(sock, buff+i, 1, 0);
		}
	}

	char post[] = "<br>';</script>\n";
	send(sock, post, sizeof(post), 0);

	return;
}