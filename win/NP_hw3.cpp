#include <windows.h>
#include <list>
using namespace std;

#include "resource.h"
#include "Http.h"
#include "Worker.h"

#define SERVER_PORT 7799

#define WM_SOCKET_NOTIFY (WM_USER + 1)

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);
//=================================================================
//	Global Variables
//=================================================================
HttpServer httpServ;
map<SOCKET, Worker> workers;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;

	int err;

	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY: {
			SOCKET ssock = wParam;
			if (WSAGETSELECTERROR(lParam))
			{
				EditPrintf(hwndEdit, TEXT("=== Error: get select error %d===\r\n"), WSAGETSELECTERROR(lParam));
				closesocket(ssock);
				return 0;
			}
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					httpServ.Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, httpServ.Socks.size());
					err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY, FD_CLOSE | FD_READ);

					if (err == SOCKET_ERROR) {
						EditPrintf(hwndEdit, TEXT("=== Error: WSAAsyncSelect %d ===\r\n"),WSAGetLastError());
						closesocket(ssock);
						WSACleanup();
						return TRUE;
					}
					break;
				case FD_READ: {
					string str;
					EditPrintf(hwndEdit, TEXT("FD_READ from ssock=%d\r\n"), ssock);
					while (1) {
						char buff[257] = {};
						int rc = recv(ssock, buff, 256, 0);
						int err = WSAGetLastError();
						if (rc == SOCKET_ERROR || rc == 0) {
							return TRUE;
						}
						else {
							str += buff;
							if (rc != 256)break;
						}
					}
					Request req;
					int r;
					if ((r=TryParseRequest(str, req)) != 0) {
						EditPrintf(hwndEdit, TEXT("Error parse request %d"),r);
						return TRUE;
					}
					workers[ssock] = Worker(ssock, req);
					workers[ssock].Init();
					for (const auto& serv : workers[ssock].servers) {
						EditPrintf(hwndEdit, TEXT("Adding csock=%d\r\n"), serv.second.sock);
						int err = WSAAsyncSelect(serv.second.sock, hwnd, WM_CGI_NOTIFY, FD_CONNECT | FD_WRITE | FD_CLOSE | FD_READ);
						if (err == SOCKET_ERROR)
							EditPrintf(hwndEdit, TEXT("=== Error: WSAAsyncSelect Connect %d ===\r\n"), WSAGetLastError());
					}
					return TRUE;
				}
					break;
				case FD_WRITE:
					EditPrintf(hwndEdit, TEXT("FD_WRITE from ssock=%d\r\n"), ssock);
					break;
				case FD_CLOSE:
					EditPrintf(hwndEdit, TEXT("FD_CLOSE from ssock=%d\r\n"), ssock);
					closesocket(ssock);
					break;
			};
			break;
		}
		case WM_CGI_NOTIFY:
		{
			
			SOCKET ssock = wParam;
			auto it = workers.begin();
			for (; it != workers.end(); ++it) {
				bool find = false;
				for (auto itt : it->second.servers) {
					if (itt.second.sock == ssock) {
						find = true;
						break;
					}
				}
				if (find)break;
			}
			if (it == workers.end())return TRUE;
			Worker& worker = it->second;
			EditPrintf(hwndEdit, TEXT("conns=%d\r\n"), worker.conns);
			if (WSAGETSELECTERROR(lParam))
			{
				EditPrintf(hwndEdit, TEXT("WSAGETSELECTERROR csock=%d %d\r\n"), ssock, WSAGETSELECTERROR(lParam));
				worker.Close(ssock);
				return 0;
			}
			switch (WSAGETSELECTEVENT(lParam))
			{
			case FD_CONNECT: 
				EditPrintf(hwndEdit, TEXT("FD_CONNECT from csock=%d\r\n"), ssock);
				worker.Connect(ssock);
				return TRUE;
			case FD_READ: 
				EditPrintf(hwndEdit, TEXT("FD_READ from csock=%d\r\n"), ssock);
				worker.Read(ssock);
				return TRUE;
			case FD_WRITE:
				EditPrintf(hwndEdit, TEXT("FD_WRITE from csock=%d\r\n"), ssock);
				worker.Write(ssock);
				return TRUE;
			case FD_CLOSE:
				EditPrintf(hwndEdit, TEXT("FD_CLOSE from csock=%d\r\n"), ssock);
				worker.Close(ssock);
				return TRUE;
			};
		}
			break;
		default:
			return FALSE;


	};

	return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
     TCHAR   szBuffer [1024] ;
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}